package todoify.taskservice.controller;
import java.util.ArrayList;
import java.util.*;

import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import todoify.taskservice.models.TodoifyReplyMessage;
import todoify.taskservice.service.*;
import todoify.taskservice.clients.BotServiceClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;


@RestController
@EnableFeignClients
public class TaskController {

    @Autowired
    private BotServiceClient botServiceClient;

    @Autowired
    private AddService addService;

    @Autowired
    private RemoveService removeService;

    @Autowired
    private UpdateService updateService;

    @Autowired
    private ViewService viewService;



    @GetMapping("/handler")
    @ResponseStatus(value = HttpStatus.OK)
    public void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument,
                            @RequestParam("replyToken") String replyToken,@RequestParam("userId") String userId) {
        Message message;
        switch (query) {
            case "/add":
                message = addService.handle(argument,userId);
                break;
            case "/remove":
                message = removeService.handle(argument);
                break;
            case "/update":
                message = updateService.handle(argument);
                break;
            case "/view":
                message = viewService.handle(argument);
                break;
            default:
                message = new TextMessage("An Error occured, Sorry I cannot process your query...");
                break;

        }
        this.sendMessage(replyToken, message);
    }

    public void sendMessage(String replyToken, Message message) {
        TodoifyReplyMessage replyMessage = new TodoifyReplyMessage(replyToken, message);
        botServiceClient.send(replyMessage);
    }


}

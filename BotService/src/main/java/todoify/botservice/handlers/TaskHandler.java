package todoify.botservice.handlers;

import todoify.botservice.clients.TaskClient;
import todoify.botservice.exception.QueryErrorHandler;

import java.util.ArrayList;

public class TaskHandler extends QueryHandler{
    private TaskClient taskClient;

    public TaskHandler(ArrayList<String> acceptedQuery, TaskClient taskClient) {
        super(acceptedQuery);
        this.taskClient = taskClient;
    }

    @Override
    public void handleQuery(String query, String argument, String replyToken,String userId) throws QueryErrorHandler {
        if (this.acceptedQuery.contains(query)) {
            this.taskClient.handleQuery(query, argument, replyToken,userId);
        } else {
            String errorMessage = "Query " + query + " is invalid...";
            throw new QueryErrorHandler(errorMessage);
        }
    }

}
